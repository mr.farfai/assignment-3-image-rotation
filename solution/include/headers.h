#include <stdio.h>

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS_NUMBER,
    READ_INVALID_HEADER,
    READ_INVALID_ERROR,
    READ_INVALID_DIMENSIONS,
    READ_INVALID_FILE_SIZE,
    MEMORY_ALLOCATION_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};


struct image;

enum read_status from_bmp(FILE* in, struct image* img);
enum write_status to_bmp(FILE* out, struct image const* img);

enum read_status from_g24(FILE* in, struct image* img);
enum write_status to_g24(FILE* out, struct image const* img);

