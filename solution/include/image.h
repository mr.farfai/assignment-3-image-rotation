#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct __attribute__((packed)) image {
    uint64_t width, height;
    struct pixel* data;
};

struct image init_image(uint64_t w, uint64_t h);

void free_image(struct image* img);
