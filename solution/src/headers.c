#include <../include/bmp_headers.h>

static uint64_t calc_padding(uint64_t w) {
    uint64_t bytes = w * 3;
    return (4 - bytes % 4) % 4;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;

    fread(&header, sizeof(struct bmp_header), 1, in);

    if (header.bfType != BMP_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biSize != HEADER_SIZE) {
        return READ_INVALID_HEADER;
    }

    if (header.biBitCount != HEADER_BIT_COUNT) {
        return READ_INVALID_BITS_NUMBER;
    }

    *img = init_image(header.biWidth, header.biHeight);;

    size_t skip = fseek(in, (long) header.bOffBits, SEEK_SET);

    if (skip) {
        free_image(img);
        return READ_INVALID_BITS_NUMBER;
    }

    size_t padding = calc_padding(img->width);

    for (size_t i = 0; i < img->height; i++) {

        size_t read = fread(img->data + i * img->width, sizeof(struct pixel), img->width, in);

        if (read != img->width) {
            free_image(img);
            return READ_INVALID_BITS_NUMBER;
        }

        int shift = fseek(in, (long) padding, SEEK_CUR);

        if (shift) {
            free_image(img);
            return READ_INVALID_BITS_NUMBER;
        }
    }

    return READ_OK;
}


enum write_status to_bmp(FILE* out, struct image const* img) {

    size_t padding = calc_padding(img->width);
    size_t img_size = (img->width + padding) * img->height;
    struct bmp_header header = {
            .bfType = BMP_SIGNATURE,
            .bfileSize = sizeof(struct bmp_header) + img_size * sizeof(struct pixel),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = HEADER_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = HEADER_BIT_COUNT,
            .biCompression = 0,
            .biSizeImage = 0,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };

    size_t write_header = fwrite(&header, sizeof(struct bmp_header), 1, out);

    if (write_header != 1) {
        return WRITE_ERROR;
    }

    uint32_t padding_space = 0;

    for (size_t i = 0; i < img->height; i++) {

        size_t write_pixels = fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);
        size_t write_padding = fwrite(&padding_space, sizeof(int8_t), padding, out);

        if (write_pixels != img->width || write_padding != padding) {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}


enum read_status from_g24(FILE* in, struct image* img) {
    uint16_t width, height;
    if (fread(&width, sizeof(uint16_t), 1, in) != 1) return READ_INVALID_ERROR;
    if (fread(&height, sizeof(uint16_t), 1, in) != 1) return READ_INVALID_ERROR;

    if (width == 0 || height == 0) {
        return READ_INVALID_DIMENSIONS;
    }

    fseek(in, 0, SEEK_END);
    long fileSize = ftell(in);
    fseek(in, 4, SEEK_SET);
    if (fileSize < 4 + width * height * 3) {
        return READ_INVALID_FILE_SIZE;
    }

    *img = init_image(width, height);

    uint8_t bgr[3];
    for (int32_t i = height - 1; i >= 0; --i) {
        for (uint32_t j = 0; j < width; ++j) {
            if (fread(bgr, 1, 3, in) != 3) return READ_INVALID_ERROR;
            img->data[i * width + j] = (struct pixel) {bgr[2], bgr[1], bgr[0]};
        }
    }

    return READ_OK;
}


enum write_status to_g24(FILE* out, const struct image* img) {
    if (fwrite(&img->width, sizeof(uint16_t), 1, out) != 1) return WRITE_ERROR;
    if (fwrite(&img->height, sizeof(uint16_t), 1, out) != 1) return WRITE_ERROR;

    for (uint32_t i = img->height; i > 0; --i) {
        for (uint32_t j = 0; j < img->width; ++j) {
            uint32_t index = (i - 1) * img->width + j;
            uint8_t bgr[3] = {img->data[index].b, img->data[index].g, img->data[index].r};
            if (fwrite(bgr, 1, 3, out) != 3) return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}
