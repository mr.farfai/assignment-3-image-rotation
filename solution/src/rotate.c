#include <../include/rotate.h>

static void transpose(struct image* img) {

    struct pixel* transposed_image = malloc(img->width * img->height * sizeof (struct pixel));

    for (uint64_t i = 0; i < img->height; i++) {
        for (uint64_t j = 0; j < img->width; j++) {
            transposed_image[j * img->height + i] = img->data[i * img->width + j];
        }
    }

    free_image(img);
    img->data = transposed_image;

    uint64_t t = img->width;
    img->width = img->height;
    img->height = t;

    for (uint64_t i = 0; i < img->height; i++) {
        for (uint64_t j = 0; j < img->width / 2; j++) {
            struct pixel tmp = img->data[i * img->width + j];
            img->data[i * img->width + j] = img->data[i * img->width + (img->width - j - 1)];
            img->data[i * img->width + (img->width - j - 1)] = tmp;
        }
    }
}


enum rotation_result rotate(struct image* img, int64_t angle) {

    angle = (360 - angle) % 360 / 90;
    for (int64_t i = 0; i < angle; i++) {
        transpose(img);
    }

    return ROTATION_OK;
}
