#include <../include/headers.h>
#include <../include/rotate.h>
#include <string.h>

int main(int argc, char** argv) {
    if (argc != 4) {
        fprintf(stderr, "Usage: <source-image> <destination-image> <angle>\n");
        return -1;
    }

    char* input_image_path = argv[1];
    char* output_image_path = argv[2];
    int64_t angle = strtoll(argv[3], NULL, 10);

    char* extension = strrchr(input_image_path, '.');
    if (!extension || extension == input_image_path) {
        fprintf(stderr, "No file extension found.\n");
        return -1;
    }

    struct image img;
    enum read_status status;

    FILE* input_image = fopen(input_image_path, "rb");
    if (!input_image) {
        fprintf(stderr, "Error opening input image file.\n");
        return -1;
    }

    if (strcmp(extension, ".bmp") == 0) {
        status = from_bmp(input_image, &img);
    } else if (strcmp(extension, ".g24") == 0) {
        status = from_g24(input_image, &img);
    } else {
        fprintf(stderr, "Unsupported file format.\n");
        fclose(input_image);
        return -1;
    }

    if (status != READ_OK) {
        fprintf(stderr, "Error reading image file.\n");
        fclose(input_image);
        return -1;
    }
    fclose(input_image);

    enum rotation_result rotation_result = rotate(&img, angle);
    if (rotation_result != ROTATION_OK) {
        fprintf(stderr, "Error rotating image.\n");
        return -1;
    }

    FILE* output_image = fopen(output_image_path, "wb");
    if (!output_image) {
        fprintf(stderr, "Error opening output image file.\n");
        return -1;
    }
    enum write_status write_status;
    if (strcmp(extension, ".bmp") == 0) {
        write_status = to_bmp(output_image, &img);
    } else if (strcmp(extension, ".g24") == 0) {
        write_status = to_g24(output_image, &img);
    } else {
        fprintf(stderr, "Unsupported file format.\n");
        fclose(output_image);
        return -1;
    }
    free_image(&img);

    if (write_status != WRITE_OK) {
        fprintf(stderr, "Error writing image file.\n");
        fclose(output_image);
        return -1;
    }
    fclose(output_image);

    return 0;
}
