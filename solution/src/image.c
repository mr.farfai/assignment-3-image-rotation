#include <../include/image.h>

struct image init_image(uint64_t w, uint64_t h) {

    struct image img;

    img.data = (struct pixel*)calloc(sizeof(struct pixel), w * h);

    img.width = w;
    img.height = h;

    return img;
}

void free_image(struct image* img) {
    free(img->data);
    img->data = NULL;
}
